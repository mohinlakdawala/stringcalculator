<?php declare(strict_types=1);

Class StringCalculator
{
    function add(string $input): int
    {
        if (! strlen($input)) {
            return 0;
        }

        // For default case, when string does not specify delimiter
        $delimiter = ',';
        $string = $input;
        
        // Attempt to split the string with delimiter specification
        $splitString = preg_split("/^\/\/(.+)\\\\n/", $input, 2, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
        
        // For the case when string does specify a custom delimiter
        if (count($splitString) == 2) {
            $delimiter = $splitString[0];
            $string = $splitString[1];
        }

        // Ignore new line characters in the string
        $string = str_replace('\n', '', $string);
        $numbers = explode($delimiter, $string);
        $sum = 0;
        $negativeNumbers = [];
        
        foreach ($numbers as $number) {
            // Capture if negative numbers
            if ($number < 0) {
                $negativeNumbers[] = $number;
                continue;
            }

            // Ignore numbers greater than 1000
            if ($number > 1000) {
                continue;
            }

            // Calculate sum
            $sum = $sum + $number;
        }

        // If string contains negative numbers, throw an exception with the list of such numbers
        if (count($negativeNumbers)) {
            throw new InvalidArgumentException('Invalid input. Negative numbers found => ' . implode(', ', $negativeNumbers));
        }

        return $sum;
    }
}
