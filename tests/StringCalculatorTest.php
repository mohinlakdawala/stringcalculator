<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class StringCalculatorTest extends TestCase
{
    public function testStringCalculatorOutputsCorrectResults(): void
    {
        $string = new StringCalculator();

        $this->assertSame(0, $string->add(''));
        $this->assertSame(8, $string->add('1,2,5'));
        $this->assertSame(6, $string->add('1\n,2,3'));
        $this->assertSame(7, $string->add('1,\n2,4'));
        $this->assertSame(8, $string->add('//;;\n1;;3;;4'));
        $this->assertSame(5, $string->add('//;;\n1;;1003;;4'));
    }

    public function testStringCalculatorThrowsExceptionWithNegativeNumbers(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid input. Negative numbers found => -1003, -4');
        $string = new StringCalculator();
        $string->add('//;;\n1;;-1003;;-4');
    }
}
