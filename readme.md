# String calculator

## Prerequisites:
- PHP8
- [Composer](https://getcomposer.org/)

## Steps to run:
- Clone the repository
- From the root of the project, install composer packages (mainly the PHPUnit package) by running the command `composer install`
- Once installed, we can run the tests via PHPUnit `./vendor/bin/phpunit tests`